import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Grid1Component } from './components/grid1/grid1.component';
import { Grid2Component } from './components/grid2/grid2.component';
import { Grid3Component } from './components/grid3/grid3.component';
import { Grid4Component } from './components/grid4/grid4.component';
import { Grid5Component } from './components/grid5/grid5.component';
import { Grid6Component } from './components/grid6/grid6.component';
import { Grid7Component } from './components/grid7/grid7.component';
import { Grid8Component } from './components/grid8/grid8.component';
import { Grid9Component } from './components/grid9/grid9.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    Grid1Component,
    Grid2Component,
    Grid3Component,
    Grid4Component,
    Grid5Component,
    Grid6Component,
    Grid7Component,
    Grid8Component,
    Grid9Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
